package com.selenium.gmail.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.selenium.gmail.base.basetest;
import com.selenium.gmail.util.datautility;

public class loginTest extends basetest {

	@BeforeClass
	public void initialize() throws Exception{
	init();	
	test = rep.startTest("loginTest");
	test.log(LogStatus.INFO, "Starting the Login test");
	datautility.setExcelFile(prop.getProperty("path"), "LoginTests");
	}
	@DataProvider(name = "loginTests")
	public Object[][] dataProvider() {
		Object[][] testData = datautility.getTestData("valid_Login");
		return testData;
	}
	@Test
	public void dologinTest() throws Exception {
		openBrowser();
		navigate("gmail_url");
		test.log(LogStatus.INFO, "enter username and password");
		test.log(LogStatus.INFO, "username is " + prop.getProperty("username") + " password is " + prop.getProperty("password"));
		doLogin();
		Thread.sleep(3000);
		System.out.println("yes");
		test.log(LogStatus.PASS, "login successfull");
		takeScreenShot();
	}
	@AfterClass
	public void tearDown() throws Exception {
		rep.endTest(test);
		rep.flush();
	    driver.close();
	}
}
