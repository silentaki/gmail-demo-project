package com.selenium.gmail.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.selenium.gmail.base.basetest;
import com.selenium.gmail.util.datautility;

public class SearchEmail extends basetest {
	@BeforeClass
	public void initialize() throws Exception{
	init();	
	test = rep.startTest("SearchEmail");
	test.log(LogStatus.INFO, "Starting the searchMail test");
	datautility.setExcelFile(prop.getProperty("path"), "LoginTests");
	}
	@DataProvider(name = "loginTests")
	public Object[][] dataProvider() {
		Object[][] testData = datautility.getTestData("valid_Login");
		return testData;
	}
	@Test(dataProvider="loginTests")
	public void doSearchTest(String username, String password) throws Exception {
		openBrowser();
		navigate("gmail_url");
		test.log(LogStatus.INFO, "enter username and password " + username+ " " + password);
		doLogin();
		test.log(LogStatus.INFO, "login successfull");
		doSearch();
		Thread.sleep(4000);	
		test.log(LogStatus.PASS, "search successfull");
		takeScreenShot();
	}
	@AfterClass
	public void tearDown() throws Exception {
		rep.endTest(test);
		rep.flush();
	    driver.quit();
	}

}
