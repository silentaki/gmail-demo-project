package com.selenium.gmail.testcases;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.selenium.gmail.base.basetest;
import com.selenium.gmail.util.datautility;

public class attachdoc extends basetest {

	@BeforeClass
		public void initialize() throws Exception{
		init(); 
		test = rep.startTest("attachdoc");
		test.log(LogStatus.INFO, "Starting the attachdoc test");
		datautility.setExcelFile(prop.getProperty("path"), "SendMail");
		}
		@DataProvider(name = "SendMail")
		public Object[][] dataProvider() {
			Object[][] testData = datautility.getTestData("valid_Login");
			return testData;
		}
		@Test(dataProvider="SendMail")
		public void attachdocTest(String Email, String Suject, String filepath) throws Exception {
			test.log(LogStatus.INFO, "Open the browser :  mozzila");
			openBrowser();
			navigate("gmail_url");
			test.log(LogStatus.INFO, "enter username and password");
			doLogin();
			test.log(LogStatus.INFO, "click on compose");
			click("btn_compose");
			test.log(LogStatus.INFO, "enter the email id to whom you want to send the mail");
			type("mailTextBox",Email);
			test.log(LogStatus.INFO, "enter the text in subject");
			type("subjectTextBox",Suject);
			test.log(LogStatus.INFO, "click attach file");
			click("btn_attachfile");
			
			
			//Using Robot
			
			StringSelection ss = new StringSelection(filepath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			
			// Ctrl + v
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);
			
			
			//Using AutoIT
			/*
			Thread.sleep(2000);
			test.log(LogStatus.INFO, "attach the file");
			Runtime.getRuntime().exec("E:\\new_workspace\\gmail_demo\\scripts\\fileupload.exe");
			Thread.sleep(2000);
			*/
			driver.findElement(By.xpath("//div[text()='Send']")).click();
			test.log(LogStatus.PASS, "mail sent successfull");
			takeScreenShot();
			
			
		}
		@AfterClass
		public void tearDown() throws Exception {
			rep.endTest(test);
			rep.flush();
		    driver.quit();
		}
		

	}

