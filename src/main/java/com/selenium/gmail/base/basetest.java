package com.selenium.gmail.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.selenium.gmail.util.ExtentManager;

public class basetest {
	 public static WebDriver driver;
	 public static Properties prop;
	 public ExtentReports rep = ExtentManager.getInstance();
	 public ExtentTest test;
	 //initialize the property
	 public static void init() throws Exception{
	    prop = new Properties();
	    
			try {
				FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"//configfolder//config.properties");
					prop.load(fs);
				} catch (IOException e) {
				
					e.printStackTrace();
				}
	 }
	 //Browser properties
			public void openBrowser() throws Exception{
				test.log(LogStatus.INFO, "open browser");
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
				test.log(LogStatus.INFO, "browser name : mozilla");
						
			}
			
			//navigate to the site	
			public void navigate(String url_xpath){
				test.log(LogStatus.INFO, "naviagting to www.gmail.com");
				driver.get(prop.getProperty(url_xpath));
			}
			
			//click properties
			public void click(String xpathEleKey){
			driver.findElement(By.xpath(prop.getProperty(xpathEleKey))).click();
			}
			
             //type properties
			public void type(String xpathElekey ,String data){
				driver.findElement(By.xpath((prop.getProperty(xpathElekey)))).sendKeys(data);;
			}
			
	//logout
	public static void logout(){
		driver.findElement(By.xpath(prop.getProperty("logoutlink"))).click();
		driver.findElement(By.xpath(prop.getProperty("btn_logout"))).click();
	}
	
	//explicit wait
	public void waitforelement(String locator , int timeout){
		try{
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
			System.out.println("element found");
			
		}catch(Exception e){
			System.out.println("element not found");
		}
				
	}
	
	public void takeScreenShot(){
		Date d = new Date();
		String filename = d.toString().replace(":","_").replace(" ","_")+".png";
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try
		{
		FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//screenshots//"+filename));
		}
		catch(Exception e)
		{
			
		}
		
		test.log(LogStatus.INFO,"Screenshot->"+test.addScreenCapture(System.getProperty("user.dir")+"//screenshots//"+filename));
	}
	/****************************************app function *********************************/
	
	
	public boolean doLogin(){
   	 type("logintextbox",prop.getProperty("email"));
   	 click("btn_next");
   	 type("passwordtextbox",prop.getProperty("Password"));
   	 click("btn_login");
   	   return false;
    }
	
	public boolean doSearch(){	 
	   	 type("searchlink","shreya");
	   	 click("btn_search");
	   	  return false;
	    }
	
	
	
	
}
